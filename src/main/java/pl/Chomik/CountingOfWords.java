package pl.Chomik;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CountingOfWords {

    private Map<String, Integer> map = new HashMap<>();

    public int numberOfTheSameWords(String str) {
        return map.getOrDefault(str, 0);
//        if (map.containsKey(str)) {
//            return map.get(str);
//        }
//        return 0;
    }

    public void addStrng(String str) {
        map.put(str, numberOfTheSameWords(str) + 1);
//        if (map.containsKey(str)){
//            map.put(str,map.get(str) + 1);
//        } else {
//            map.put(str,1);
//        }
    }
}
