package pl.Chomik;

import org.junit.Assert;
import org.junit.Test;

public class CountingOfWordsTest {

    private final CountingOfWords subject = new CountingOfWords();
    @Test
    public void shouldReturnZeroFromUnusedWord() {
        //given
        String str = "koteł";

        //when
        int result = subject.numberOfTheSameWords(str);

        //then
        Assert.assertEquals(0,result);
    }

    @Test
    public void shouldReturnOneIfAddedNonExistentWord() {
        //given
        String str = "koteł";
        subject.addStrng(str);

        //when
        int result = subject.numberOfTheSameWords(str);

        //then
        Assert.assertEquals(1,result);
    }

    @Test
    public void shouldReturnCountOfUsedWords() {
        //given

        String str = "koteł";

        subject.addStrng(str);
        subject.addStrng(str);

        //when
        int result = subject.numberOfTheSameWords(str);

        //then
        Assert.assertEquals(2, result);


    }

    @Test
    public void shouldReturnCountOfInsertedWords() {
        //given
        String str = "kot";
        String str1 = "pieseł";
        String str2 = "rybeł";

        subject.addStrng(str);
        subject.addStrng(str);
        subject.addStrng(str1);
        subject.addStrng(str2);

        //when
        int result = subject.numberOfTheSameWords(str);

        //then
        Assert.assertEquals(2, result);
    }
}
